# Changelog
#### All notable changes to this project will be documented in this file.
## Repositorio de prácticas de sii 2020/2021
### Elena Cabrera Casquet

#### V1.5 2020-12-23
**Added** Comunicación servidor - cliente mediante sockets

#### V1.4 2020-12-17
**Added** Comunicación servidor - cliente mediante tuberías

#### V1.3 2020-12-01
---

**Added:** Marcador de puntuaciones mediante logger
**Added:** Bot jugador 1
**Added:** Fin del juego al llegar a 3 ptos

#### V1.2 2020-10-29
---

**Added:** Movimiento a la esfera y disminuye su tamaño con el tiempo hasta radio=0.1

**Added:** Movimiento a las raquetas

#### V1.1 2020-10-13
---

**Added:** Datos en las cabeceras
