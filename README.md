*Desarrollado por Elena Cabrera Casquet en las prácticas de laboratorio de la asignatura de Sistemas Informáticos Industriales del grado Ingeniería Electrónica Industrial y Automática - 2020*
# Tenis

Tenis es un videojuego desarrollado en C++ y compatible con el Sistema Operativo Linux.

## Ejecución

1. Crear un directorio build dentro de workspace: `mkdir workspace/build
2. Hacer cmake desde build: `cd workspace/build` `cmake ..`
3. Hacer make desde build: `make`
4. Ejecutar el logger y tenis en ese orden: `src/logger` `src/tenis`
5. Opcional: ejecutar el bot `src/bot` para que juegue como el jugador 1

## Instrucciones del juego

*Mover la raqueta izquierda: teclas `w` y `s`, en caso de dos jugadores humanos
*Mover la raqueta derecha: teclas `o` y `l`
*Fin del juego: gana el jugador que alcance 3 puntos

## License
**Copyright (c) 2020 Elena Cabrera Casquet**

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

