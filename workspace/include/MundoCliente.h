// Nombre: Elena Cabrera Casquet
// Nmat: 53854
//////////////////////////////////////////////////////////////////////


#include <vector>
#include "Plano.h"
#include "glut.h"
#include "Socket.h"

#pragma once

#include "Esfera.h"
//#include "Raqueta.h"

#include "Puntuaciones.h"
#include "DatosMemCompartida.h"

class CMundoCliente  
{
public:
	void Init();
	CMundoCliente();
	virtual ~CMundoCliente();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();	

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	puntuaciones score;
	DatosMemCompartida datos_comp;
	DatosMemCompartida *pdatos_comp;
	
	Socket comunicacion; //socket para comunicación con el servidor
	
	int fd_bot; //fd del bot
};
