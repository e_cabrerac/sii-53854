// Nombre: Elena Cabrera Casquet
// Nmat: 53854
//////////////////////////////////////////////////////////////////////

#pragma once
#include <vector>
#include "Plano.h"
#include "glut.h"
#include "Esfera.h"
#include "Puntuaciones.h"
#include "DatosMemCompartida.h"
#include "Socket.h"

class CMundoServidor  
{
public:
	void Init();
	CMundoServidor();
	virtual ~CMundoServidor();	
	
	void InitGL();	
	void OnKeyboardDown(unsigned char key, int x, int y);
	void OnTimer(int value);
	void OnDraw();
	void RecibeComandosJugador();

	Esfera esfera;
	std::vector<Plano> paredes;
	Plano fondo_izq;
	Plano fondo_dcho;
	Raqueta jugador1;
	Raqueta jugador2;

	int puntos1;
	int puntos2;
	puntuaciones score;
	DatosMemCompartida datos_comp;
	DatosMemCompartida *pdatos_comp;
	
	//Declaración sockets conexión y comunicación con el cliente
	Socket conexion;
	Socket comunicacion;
	
	int fd; //fd de la tubería
	
	pthread_t thid1; //thread para evitar bloqueo a la espera de pulsar una tecla
};

