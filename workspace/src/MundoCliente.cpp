// Nombre: Elena Cabrera Casquet
// Nmat: 53854
//////////////////////////////////////////////////////////////////////
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundoCliente::CMundoCliente()
{
	Init();
}

CMundoCliente::~CMundoCliente()
{
}

void CMundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	esfera.Dibuja();

	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundoCliente::OnTimer(int value)
{	
	//Lectura coordenadas recibidas del servidor
	char cad[200];
	comunicacion.Receive(cad, sizeof(cad));
	
	//Actualización de los atributos con los datos recibidos del servidor
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d %f", &esfera.centro.x,&esfera.centro.y, 		&jugador1.x1, &jugador1.y1, &jugador1.x2,& jugador1.y2, &jugador2.x1, 		&jugador2.y1, &jugador2.x2, &jugador2.y2, &puntos1, &puntos2, &esfera.radio); 
	
	
	//Datos memoria compartida
	pdatos_comp->esfera = esfera;
	pdatos_comp->raqueta1 = jugador1;
	if(pdatos_comp->accion == 1)
		OnKeyboardDown('w', 0, 0); //subir
	else if (pdatos_comp->accion == -1)
		OnKeyboardDown('s', 0, 0); //bajar
	//para el caso de accion == 0 no se hace nada
	if(puntos1>=3 || puntos2 >=3)
		pdatos_comp->fin=true;
}

void CMundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
	char k = 0;
	switch(key){
	case 's' : k='s'; break;
	case 'w' : k='w'; break;
	case 'l' : k='l'; break;
	case 'o' : k='o'; break;
	}
	comunicacion.Send(&k, sizeof(key));
}

void CMundoCliente::Init()
{
	//Nombre para enviar al servidor
	char name[25];
	printf("Introduzca identificador: ");
	scanf("%s", name);
	//Conexión con el servidor
	char ip[] = "127.0.0.1";
	comunicacion.Connect(ip, 4200);
	//Envío del nombre
	comunicacion.Send(name, sizeof(name));
	
	
	//Inicialización datos memoria compartida
	datos_comp.esfera=esfera;
	datos_comp.raqueta1=jugador1;
	datos_comp.accion=0;
	//Fichero memoria compartida del tamaño de datos compartidos
	if ((fd_bot=open("DatosCompartidos", O_CREAT|O_RDWR|O_TRUNC, 0666))<0) {
		perror("ERROR creación fichero destino\n");
		return;
	}
	write(fd_bot, &datos_comp, sizeof(datos_comp));
	if ((pdatos_comp=static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(datos_comp), PROT_READ|PROT_WRITE, MAP_SHARED, fd_bot, 0)))==MAP_FAILED){
		perror("Error en la proyeccion del fichero");
		close(fd_bot);
		return;
	}
	close(fd_bot);
	
	
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
}
