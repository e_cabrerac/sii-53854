//Ficheros de cabecera
#include"DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <sys/mman.h>
#include <unistd.h>

int main(void) {
	DatosMemCompartida *pdatos;
	int fd;
	
	if((fd = open("DatosCompartidos", O_RDWR)) < 0) {
		perror("Error abriendo DatosCompartidos");
		return 1;
	}
	
	if ((pdatos=static_cast<DatosMemCompartida*>(mmap(NULL, sizeof(DatosMemCompartida), PROT_READ|PROT_WRITE, MAP_SHARED, fd, 0)))==MAP_FAILED){
		perror("Error en la proyeccion del fichero");
		close(fd);
		return 1;
	}
	close(fd);
	
	//Bucle infinito
	while(1) {
		//lógica
		if(pdatos->esfera.centro.y > pdatos->raqueta1.Centro())
			pdatos->accion = 1;
		else if(pdatos->esfera.centro.y < pdatos->raqueta1.Centro())
			pdatos->accion = -1;
		else
			pdatos->accion = 0;
		//suspensión 25ms	
		usleep(25000);
		if(pdatos->fin)
			break;
	}
	
	return 0;
}
