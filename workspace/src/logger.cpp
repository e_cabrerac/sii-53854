//Ficheros de cabecera
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <fcntl.h>
#include <unistd.h>
#include "Puntuaciones.h"
int main(void){
	int fd;
	puntuaciones p;
	
	//Creación del FIFO
	if (mkfifo("FIFO", 0600)<0) {
		perror("ERROR creando el FIFO");
		return(1);
	}
	
	//Abrir el FIFO
	if ((fd=open("FIFO", O_RDONLY))<0) {
		perror("ERROR abriendo el FIFO");
		return(1);
	}
	
	//Bucle infinito recepción de datos
	while (read(fd, &p, sizeof(p))==sizeof(p)) {
		//Imprimir el mensaje por salida estándar
		if(p.who == false)
			printf("Jugador 1 marca 1 punto, lleva un total de %d puntos.\n", p.player1);
		else
			printf("Jugador 2 marca 1 punto, lleva un total de %d puntos.\n", p.player2);
	}
	
	//Cierre del fd y del FIFO
	close(fd);
	unlink("FIFO");
	
	return(0);
}
